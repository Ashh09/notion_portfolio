# Alyssa Ashley Mendoza

I am a software developer based in the Philippines with less than a year experience in the software industry.

My focus area is mobile development and I have been using flutter platform for quite sometime now, I am also doing Odoo development which is a suite of business management software tools. I am very open in learning new things that can help me grow in my software development career.

# Contact Information

LinkedIn: [www.linkedin.com/in/mendozaaac](http://www.linkedin.com/in/mendozaaac)

Website Portfolio: [https://mendoza-flutter-portfolio.herokuapp.com/#/](https://mendoza-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Developer Trainee

FFUF, from July 2021 - Present

- I am trained to do flutter development in mobile applications (IOS and Android).
- I am trained with best coding practices such as KISS,DRY, YAGNI and SOLID principles.

# Skills

### Technical Skills

- Software Development
- Programming
- Flutter / Dart
- Java
- Machine Learning
- Python

### Soft Skills

- Communication
- Problem Solving
- Critical Thinking

# Education

BS Computer Science

National University - Manila 2015 - 2021